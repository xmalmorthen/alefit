import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin'
import * as express from 'express';
import * as cors from 'cors';

const serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://alefit-97917.firebaseio.com"
});

const db = admin.firestore();

// export const getPwd = functions.https.onRequest( async (request, response) => {
//   const pwdRef  =  db.collection('pwd');
//   const docSnap =  await pwdRef.get();
//   const pwd     = docSnap.docs.map( doc => doc.data());

//   response.json( pwd[0] );
// });

// Express
const app =  express();
app.use( cors({origin: true}) );

app.get('/pwd', async (req, res) =>{
  const pwdRef  =  db.collection('pwd');
  const docSnap =  await pwdRef.get();
  const pwd     = docSnap.docs.map( doc => doc.data());

  res.json( pwd[0] );
});

app.post('/pwd/:pwd', async (req, res) =>{

  const pwd = req.params.pwd;
  const pwdRef = db.collection('pwd').doc('pwd');
  await pwdRef.update({pwd});

  res.json({
    status: true,
    message: 'Contraseña actualizada',
    data: { pwd }
  });

});


export const api = functions.https.onRequest( app );
