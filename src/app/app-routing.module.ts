import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import {  MainComponent,
          AdminComponent } from './pages/pages.index.components';

const routes: Routes = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: 'main',
    redirectTo: ''
  },
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
