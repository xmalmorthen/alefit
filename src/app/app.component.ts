import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

// services
import { GeneralSubjectsService } from './services/general-subjects.service';
import { environment } from 'src/environments/environment';

declare const $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  footerStatus: boolean = true;

  private footerStatusSubscription: Subscription;

  constructor(
    private generalSubjectsService: GeneralSubjectsService
  ){
    const rndNumber = Math.floor(Math.random() * 7) + 1;
    const imgUrl = `assets/images/backgrounds/bg_${rndNumber}.jpg`
    $('body.bg').css({'background-image':'url(' + imgUrl + ')'});
  }

  ngOnInit(){
    this.footerStatusSubscription = this.generalSubjectsService.hideFooter.subscribe( (footerStatus)=>{
      this.footerStatus = footerStatus;
    });
  }

  ngOnDestroy(){
    this.footerStatusSubscription.unsubscribe();
  }

}
