import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

// services
import { ApiService } from '../../services/api.service';
import { GeneralSubjectsService } from '../../services/general-subjects.service';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  forma: FormGroup;
  submited: boolean = false;
  submiting: boolean = false;

  constructor(
    private apiService: ApiService,
    private generalSubjectsService: GeneralSubjectsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.generalSubjectsService.setHeaderTitle('<i class="fa fa-lock" aria-hidden="true"></i> Administrar contraseña de sala');
    this.generalSubjectsService.setfooterStatus(true);

    this.forma = new FormGroup({
      pwd: new FormControl('', Validators.required)
    });
  }

  getKey(key:string) { return this.forma.get(key); }

  async setPwd(){

    this.submiting = true;
    this.submited = true;

    if (this.forma.valid ) {

      const pwd = this.getKey('pwd').value;

      const setPwdResponse: any = await this.apiService.setPwd(pwd).toPromise()
      .catch( err => {
        return { status: false, message: 'Ocurrió un error al intentar actualizar la contraseña, favor de intentarlo de nuevo!!!'};
      });

      if (!setPwdResponse.status) {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: setPwdResponse.message
        });
        return false;
      }

      Swal.fire({
        icon: 'success',
        title: `Contraseña de sala [ ${pwd} ]`,
        text: 'Actualizada correctamente!'
      }).then( () =>{
        this.router.navigate(['/'])
      });

    }

    this.submiting = false;

  }

}
