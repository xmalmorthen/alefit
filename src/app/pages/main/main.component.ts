import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

// services
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  pwd: string = '';

  constructor(
    private title: Title,
    private apiService: ApiService
  ) {
    this.title.setTitle( 'Ale Fit' );
  }

  ngOnInit() {
    this.apiService.getPwd()
    .subscribe( (pwd: any)=> {
      this.pwd = pwd.pwd;
    })
  }

  enterRoom(evt){
    window.open('https://meet.jit.si/aleclase', '_blank');
  }

}
