import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// routing
import { PagesRoutingModule } from './pages-routing.module';

// components
import { MainComponent } from './main/main.component';

// modules
import { SharedModule } from './shared/shared.module';
import { AdminComponent } from './admin/admin.component';

// services
import { GeneralSubjectsService } from '../services/general-subjects.service';


@NgModule({
  declarations: [MainComponent, AdminComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    PagesRoutingModule,
  ],
  exports:[

  ],
  providers:[
    GeneralSubjectsService
  ]
})
export class PagesModule { }
