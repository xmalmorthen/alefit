import { Component, OnInit, OnDestroy } from '@angular/core';

// services
import { GeneralSubjectsService } from '../../../services/general-subjects.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  private headerTitleSubscription: Subscription;

  headerTitle: string;

  constructor(
    private generalSubjectsService: GeneralSubjectsService
  ) { }

  ngOnInit() {
    this.headerTitleSubscription = this.generalSubjectsService.headerTitle.subscribe( (title:string)=>{
      this.headerTitle = title;
    });
  }

  ngOnDestroy(){
    this.headerTitleSubscription.unsubscribe();
  }


}
