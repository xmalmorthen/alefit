import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiEndPoint: string;

  constructor(
    private httpClient : HttpClient
  ) {
    this.apiEndPoint = environment.api;
  }

  getPwd(){
    return this.httpClient.get( `${this.apiEndPoint}/pwd`)
      .pipe(
        map( (pwd) => {
          return pwd;
        })
      );
  }

  setPwd(pwd: string){
    return this.httpClient.post( `${this.apiEndPoint}/pwd/${pwd}`,{});
  }
}
