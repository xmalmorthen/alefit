import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeneralSubjectsService {

  constructor() { }

  private _headerTitle = new BehaviorSubject<string>('<i _ngcontent-ncf-c1="" aria-hidden="true" class="fa fa-heartbeat pr-2"></i><span _ngcontent-ncf-c1="" class="title">Ale de la Vega</span>');
  public headerTitle =  this._headerTitle.asObservable();
  setHeaderTitle(title: string){
    this._headerTitle.next(title);
  }

  private _hideFooter = new BehaviorSubject<boolean>(false);
  public hideFooter =  this._hideFooter.asObservable();
  setfooterStatus(hide: boolean){
    this._hideFooter.next(hide);
  }

}
