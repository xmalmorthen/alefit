export const environment = {
  production: true,
  folder: 'aleFit',
  api: 'https://us-central1-alefit-97917.cloudfunctions.net/api'
};
